package io.drone.data.repositories;

import io.drone.enums.StateTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql({"/data.sql"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MedicationRepositoryTest {
    @Autowired
    MedicationRepository medicationRepository;

    @Test
    void medicationNotAssigned() {
        final var medications = this.medicationRepository.getNotAssignedMedication().orElse(null);

        Assertions.assertNotNull(medications);
        Assertions.assertNotEquals(0, medications.size());
    }

    @Test
    void getMedicationByDrone() {
        final var medications = this.medicationRepository.findMedicationByDrone(StateTypeEnum.LOADED.toString(), 4L).orElse(null);

        Assertions.assertNotNull(medications);
        Assertions.assertNotEquals(0, medications.size());
    }
}
