package io.drone.data.repositories;

import io.drone.enums.StateTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql({"/data.sql"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DroneRepositoryTest {
    @Autowired
    private DroneRepository droneRepository;

    @Test
    void DronesWithLoadingStatus() {
        final var drones = this.droneRepository.findByState(StateTypeEnum.LOADED);

        Assertions.assertNotNull(drones);
        Assertions.assertNotEquals(0, drones.size());
    }

    @Test
    void DronesWithStateAndBatteryLevel() {
        final var drones = this.droneRepository.findByStateAndBatteryCapacityIsGreaterThanEqualOrderByWeightLimitDesc(StateTypeEnum.LOADED, 30);

        Assertions.assertNotNull(drones);
        Assertions.assertNotEquals(0, drones.size());
    }

    @Test
    void updateDroneStatus() {
         this.droneRepository.updateDroneStateByIds(StateTypeEnum.DELIVERING.toString(), List.of(3L));
          var drone = this.droneRepository.findById(3L).orElse(null);
        Assertions.assertNotNull(drone);
        Assertions.assertEquals(StateTypeEnum.DELIVERING.toString(), drone.getState().toString());
    }
}
