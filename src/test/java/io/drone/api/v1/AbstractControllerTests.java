package io.drone.api.v1;

import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private Keycloak keycloak;

    @PostConstruct
    void init() {
        this.restTemplate.getRestTemplate().getInterceptors().add((request, body, execution) -> {
            final HttpHeaders headers = request.getHeaders();
            if (!headers.containsKey(HttpHeaders.AUTHORIZATION)) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " +
                        AbstractControllerTests.this.keycloak.tokenManager().getAccessTokenString());
            }
            return execution.execute(request, body);
        });
    }

    protected <T> T postForObject(final URI url, final Object body, final Class<T> responseType) {
        final var response = this.restTemplate.postForEntity(url, body, responseType);
        if (response.getStatusCode().isError()) {
            throw new RuntimeException(response.getStatusCode().getReasonPhrase());
        }
        return response.getBody();
    }

    protected <T> T getForObject(final URI url, final Class<T> responseType) {
        final var response = this.restTemplate.getForEntity(url, responseType);
        if (response.getStatusCode().isError()) {
            throw new RuntimeException(response.getStatusCode().getReasonPhrase());
        }
        return response.getBody();
    }

    protected void put(final URI url, final Object request) {
        this.restTemplate.put(url, request);
    }

    protected <T> T put(final URI url, final Object request, final Class<T> responseType) {
        final var httpEntity = new HttpEntity<>(request);
        final var response = this.restTemplate.exchange(url, HttpMethod.PUT, httpEntity, responseType);
        return response.getBody();
    }

    protected <T> T delete(final URI url, final Object request, final Class<T> responseType) {
        final var httpEntity = new HttpEntity<>(request);
        final var response = this.restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, responseType);
        return response.getBody();
    }

    protected <T> ResponseEntity<T> exchange(final URI url, final HttpMethod method, final HttpEntity<?> requestEntity, final Class<T> responseType) {
        return this.restTemplate.exchange(url, method, requestEntity, responseType);
    }

    protected URI apiUri(final String path, final Map<String, String> params) {
        final var httpParams = new LinkedMultiValueMap<String, String>();
        params.forEach(httpParams::add);

        return UriComponentsBuilder
                .fromHttpUrl(String.format("http://localhost:%d", this.port))
                .path(String.format("/api/v1/%s", path))
                .queryParams(httpParams)
                .build()
                .toUri();
    }

    protected URI apiUri(final String path) {
        return apiUri(path, Map.of());
    }
}
