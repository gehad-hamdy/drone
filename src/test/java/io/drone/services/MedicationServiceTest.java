package io.drone.services;

import io.drone.api.dto.DroneMedicationFlattenDTO;
import io.drone.data.entities.Drone;
import io.drone.data.entities.Medication;
import io.drone.data.repositories.DroneRepository;
import io.drone.data.repositories.MedicationRepository;
import io.drone.data.repositories.impl.MedicationRepositoryCustomImpl;
import io.drone.enums.ModelTypeEnum;
import io.drone.enums.StateTypeEnum;
import io.drone.services.exceptions.BusinessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
public class MedicationServiceTest extends CommonData{
    @InjectMocks
    MedicationService medicationService;

    @Mock
    MedicationRepository medicationRepository;

    @Test
    void addMedicationTest() {
        var testMedication = this.insertMedicationData();

        when(this.medicationRepository.save(any(Medication.class))).thenReturn(testMedication);

        var medication = this.medicationService.addMedication("ggg", 20, "123GH", null);

        Assertions.assertEquals(testMedication, medication);

        verify(this.medicationRepository, times(1)).save(any(Medication.class));
        verifyNoMoreInteractions(this.medicationRepository);
    }

    @Test
    void addMedicationWithExceptionTest() {
        Assertions.assertNull(
            this.medicationService.addMedication(anyString(), 20, "123 gh", null)
        );
    }

    @Test
    void getMedicationByDroneIdTest(){
        var testMedication = this.insertMedicationData();
        when(this.medicationRepository.findMedicationByDrone(StateTypeEnum.LOADED.toString(), 11L))
                .thenReturn(Optional.of(List.of(testMedication)));

        var medication = this.medicationService.getMedicationByDroneId(11L);
        Assertions.assertNotNull(medication);
        Assertions.assertEquals(1, medication.size());

        verify(this.medicationRepository, times(1)).findMedicationByDrone(StateTypeEnum.LOADED.toString(), 11L);
        verifyNoMoreInteractions(this.medicationRepository);
    }
}
