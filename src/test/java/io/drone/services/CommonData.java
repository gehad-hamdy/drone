package io.drone.services;

import io.drone.api.dto.DroneMedicationFlattenDTO;
import io.drone.data.entities.Drone;
import io.drone.data.entities.Medication;
import io.drone.enums.ModelTypeEnum;
import io.drone.enums.StateTypeEnum;

public class CommonData {
    public Drone insertDroneData() {
        Drone drone = new Drone();
        drone.setId(11L);
        drone.setBatteryCapacity(30);
        drone.setModel(ModelTypeEnum.Cruiserweight);
        drone.setState(StateTypeEnum.IDLE);
        drone.setSerialNumber("98765400");
        drone.setWeightLimit(30);

        return drone;
    }

    public Drone insertDroneDataWithStatusParam(StateTypeEnum state) {
        Drone drone = new Drone();
        drone.setId(11L);
        drone.setBatteryCapacity(30);
        drone.setModel(ModelTypeEnum.Cruiserweight);
        drone.setState(state);
        drone.setSerialNumber("98765400");
        drone.setWeightLimit(30);

        return drone;
    }

    public DroneMedicationFlattenDTO insertDroneWithMedicationData() {
        DroneMedicationFlattenDTO drone = new DroneMedicationFlattenDTO();
        drone.setDroneId(11L);
        drone.setBatteryCapacity(30);
        drone.setModel(ModelTypeEnum.Cruiserweight.toString());
        drone.setState(StateTypeEnum.IDLE.toString());
        drone.setSerialNumber("98765400");
        drone.setWeightLimit(30);
        drone.setMedicationId(12L);
        drone.setCode("9874566666666");
        drone.setWeight(20);
        drone.setName("test");

        return drone;
    }

    public Medication insertMedicationData() {
        Medication medication = new Medication();
        medication.setId(11L);
        medication.setCode("9874566666666");
        medication.setWeight(20);
        medication.setDrone(this.insertDroneDataWithStatusParam(StateTypeEnum.LOADED) );
        medication.setName("test");

        return medication;
    }
}
