package io.drone.services;

import io.drone.api.dto.DroneMedicationDTO;
import io.drone.api.dto.DroneMedicationFlattenDTO;
import io.drone.data.entities.Drone;
import io.drone.data.repositories.DroneLogsRepository;
import io.drone.data.repositories.DroneRepository;
import io.drone.data.repositories.impl.MedicationRepositoryCustomImpl;
import io.drone.enums.ModelTypeEnum;
import io.drone.enums.StateTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DroneServiceTest extends CommonData{
    @Mock
    DroneRepository droneRepository;

    @Mock
    MedicationRepositoryCustomImpl medicationRepositoryCustom;

    @InjectMocks
    DroneService droneService;

    @Test
    void getDroneByIdTest() {
        final var testDrone = this.insertDroneData();

        when(this.droneRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.of(testDrone));

        var data = this.droneService.getDroneById(testDrone.getId());
        Assertions.assertNotNull(data);

        verifyNoMoreInteractions(droneRepository);
    }

    @Test
    void listDroneByStateTest() {
        when(this.droneRepository.findByState(StateTypeEnum.IDLE)).thenReturn(List.of(this.insertDroneData()));

        Assertions.assertNotNull(this.droneService.listDronesByState(StateTypeEnum.IDLE.toString()));

        verify(this.droneRepository, times(1)).findByState(StateTypeEnum.IDLE);
    }

    @Test
    void listDroneAsGeneral() {
        when(this.droneRepository.findAll()).thenReturn(List.of(this.insertDroneData()));

        Assertions.assertNotNull(this.droneService.listDronesByState(null));

        verify(this.droneRepository, times(1)).findAll();
    }

    @Test
    void listDroneWithMedicationTest() {
        when(this.medicationRepositoryCustom.listDronesWithMedication())
                .thenReturn(List.of(this.insertDroneWithMedicationData()));

        Assertions.assertNotNull(this.droneService.listDronesWithMedication());

        verify(this.medicationRepositoryCustom, times(1)).listDronesWithMedication();
    }

    @Test
    void registerDroneTest() {
        final var testDrone = this.insertDroneData();

        when(this.droneRepository.save(any(Drone.class))).thenReturn(testDrone);

        var drone = this.droneService.registerDrone(new Drone());

        Assertions.assertEquals(testDrone, drone);

        verify(droneRepository, times(1)).save(any(Drone.class));
        verifyNoMoreInteractions(droneRepository);
    }
}
