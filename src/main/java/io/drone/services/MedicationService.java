package io.drone.services;

import io.drone.data.entities.Drone;
import io.drone.data.entities.Medication;
import io.drone.data.repositories.DroneRepository;
import io.drone.data.repositories.MedicationRepository;
import io.drone.data.repositories.impl.MedicationRepositoryCustomImpl;
import io.drone.enums.StateTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MedicationService {
    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    DroneRepository droneRepository;

    @Autowired
    MedicationRepositoryCustomImpl medicationRepositoryCustom;

    @Value("${ALLOWED_BATTERY_CAPACITY_TO_DELIVERED}")
    private Integer batteryCapacity;

    public Medication addMedication(String name, Integer weight, String code, MultipartFile image) {
        Medication medication = new Medication();
        medication.setName(name);
        medication.setCode(code);
        medication.setWeight(weight);

        if (image != null)
          medication.setImage(image.getOriginalFilename());

        return this.medicationRepository.save(medication);
    }

    public void assignMedicationToAvailableDrone() {
        //get all medications that not assigned to drone
        var medicationsPresent = this.medicationRepository.getNotAssignedMedication();
        if (medicationsPresent.isPresent()) {
            var medications = medicationsPresent.get();
            //get all idle drones
            var drones = this.droneRepository.findByStateAndBatteryCapacityIsGreaterThanEqualOrderByWeightLimitDesc(StateTypeEnum.IDLE, batteryCapacity);
               if (!drones.isEmpty()) {
                   List<Long> droneIds = new ArrayList<>();
                   Map<Long,Long> medicationDrone = new HashMap<>();

                   for (Drone drone : drones) {
                       var droneWeight = drone.getWeightLimit();
                       for (int index = 0; index < medications.size() - 1; index++) {
                           if (droneWeight != 0 && medications.get(index).getWeight() <= droneWeight){
                               droneWeight -= medications.get(index).getWeight();
                               medicationDrone.put(medications.get(index).getId(), drone.getId());
                               if (!droneIds.contains(drone.getId()))
                                   droneIds.add(drone.getId());

                               medications.remove(medications.get(index));
                           }
                       }
                       if (medications.isEmpty()) break;
                   }

                   //update medication by drones
                   if (!medicationDrone.isEmpty()) {
                       //update medication with assigned drone_id
                        this.medicationRepositoryCustom.updateMedicationsByDroneIds(medicationDrone);
                       //update drone state to be loading
                       this.droneRepository.updateDroneStateByIds(StateTypeEnum.LOADING.toString(), droneIds);
                   }
               }
        }
    }

    public List<Medication> getMedicationByDroneId(Long droneId) {
        return this.medicationRepository.findMedicationByDrone(StateTypeEnum.LOADED.toString(), droneId).orElse(null);
    }
}
