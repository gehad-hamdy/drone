package io.drone.services;

import io.drone.api.v1.resources.DroneRequestBodyResource;
import io.drone.helpers.NumberHelper;
import io.drone.services.exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class Validations {
    @Autowired
    private FileValidator fileValidator;

    public void validateAbstractDrone(final DroneRequestBodyResource droneRequestBodyResource) {
        String code = String.valueOf(HttpStatus.BAD_REQUEST);
        if (droneRequestBodyResource.getBatteryCapacity() == null || (
                !NumberHelper.isIntegerValue(droneRequestBodyResource.getBatteryCapacity()) &&
                (droneRequestBodyResource.getBatteryCapacity() > 100 &&
                 droneRequestBodyResource.getBatteryCapacity() < 0))) {
            throw BusinessException.badRequest("Battery Capacity must be int and less than 100 %", code);
        }
        if (droneRequestBodyResource.getSerialNumber() == null || droneRequestBodyResource.getSerialNumber().length() > 100) {
            throw BusinessException.badRequest("Serial number must be less than 100 char", code);
        }
        if (droneRequestBodyResource.getModel() == null) {
            throw BusinessException.badRequest("Model shouldn't be blank", code);
        }
        if (droneRequestBodyResource.getState() == null) {
            throw BusinessException.badRequest("State shouldn't be blank", code);
        }
        if (droneRequestBodyResource.getWeightLimit() > 500) {
            throw BusinessException.badRequest("Weight limit must be less than 500 gr", code);
        }
    }

    public void validateMedicationRequest(final String name, final String code, final MultipartFile image) {
        String statusCode = String.valueOf(HttpStatus.BAD_REQUEST);
        String regexName = "^[A-Za-z0-9_-]*$";
        String regexCode = "^[A-Z0-9_]*$";

        if (name == null ||
                this.checkValidPattern(name, regexName)){
            throw BusinessException.badRequest("Name should be allowed only letters, numbers, ‘-‘, ‘_’", statusCode);
        }
        if (code == null ||
                this.checkValidPattern(code, regexCode)){
            throw BusinessException.badRequest("Code should be allowed only upper case letters, underscore and numbers", statusCode);
        }
        if (image == null ||
                !this.fileValidator.isValid(image, null)) {
            throw BusinessException.badRequest("Image should be allowed only png,jpg,jpeg", statusCode);
        }
    }

    private boolean checkValidPattern(String name, String regex){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        return !matcher.matches();
    }
}
