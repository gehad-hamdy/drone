package io.drone.services;

import io.drone.api.dto.DroneMedicationDTO;
import io.drone.api.dto.DroneMedicationFlattenDTO;
import io.drone.api.dto.MedicationDTO;
import io.drone.data.entities.Drone;
import io.drone.data.entities.DroneLogs;
import io.drone.data.repositories.DroneLogsRepository;
import io.drone.data.repositories.DroneRepository;
import io.drone.data.repositories.impl.MedicationRepositoryCustomImpl;
import io.drone.enums.StateTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DroneService {
    @Autowired
    DroneRepository droneRepository;

    @Autowired
    MedicationRepositoryCustomImpl medicationRepositoryCustom;

    @Autowired
    DroneLogsRepository droneLogsRepository;

    public Drone registerDrone(Drone drone) {
       return  this.droneRepository.save(drone);
    }

    public Drone getDroneById(Long id) {
        return this.droneRepository.findById(id).orElseThrow();
    }

    public List<Drone> listDronesByState(String state) {
        if (state == null || state.isEmpty())
            return this.droneRepository.findAll();

       return this.droneRepository.findByState(StateTypeEnum.valueOf(state));
    }

    public List<DroneMedicationDTO> listDronesWithMedication() {
        var res = this.medicationRepositoryCustom.listDronesWithMedication();
        if (!res.isEmpty())
            return this.mapDroneByMedication(this.mapDroneById(res)) ;

        return null;
    }

    //chk battery level every 15 minutes and log
    @Scheduled(fixedRate = 900000)
    @Transactional
    public void checkDroneBatteryLevelAndLog() {
        var drones = this.droneRepository.findAll();
        if (!drones.isEmpty()) {
            List<DroneLogs> droneLogs = new ArrayList<>();

            for (Drone drone : drones) {
                DroneLogs droneLog = new DroneLogs();
                droneLog.setDrone(drone);
                droneLog.setBatteryLevel(drone.getBatteryCapacity());

                droneLogs.add(droneLog);
            }

            this.droneLogsRepository.saveAll(droneLogs);
        }
    }

    private Map<Long, List<DroneMedicationFlattenDTO>> mapDroneById(List<DroneMedicationFlattenDTO> droneMedicationFlattenDTOS) {
        if (droneMedicationFlattenDTOS != null) {
            return droneMedicationFlattenDTOS.stream().collect(Collectors.groupingBy(DroneMedicationFlattenDTO::getDroneId, Collectors.mapping(drone -> drone, Collectors.toList())));
        }

        return new HashMap<>();
    }

    private List<DroneMedicationDTO> mapDroneByMedication(Map<Long, List<DroneMedicationFlattenDTO>> drones) {
        List<DroneMedicationDTO> droneMedicationDTOS = new ArrayList<>();
        for (Map.Entry<Long, List<DroneMedicationFlattenDTO>> drone: drones.entrySet()) {
            DroneMedicationDTO droneMedicationDTO = new DroneMedicationDTO();
            droneMedicationDTO.setId(drone.getKey());
            droneMedicationDTO.setSerialNumber(drone.getValue().get(0).getSerialNumber());
            droneMedicationDTO.setBatteryCapacity(drone.getValue().get(0).getBatteryCapacity());
            droneMedicationDTO.setModel(drone.getValue().get(0).getModel());
            droneMedicationDTO.setState(drone.getValue().get(0).getState());
            droneMedicationDTO.setWeightLimit(drone.getValue().get(0).getWeightLimit());

            List<MedicationDTO> medicationDTOList = new ArrayList<>();

            for (DroneMedicationFlattenDTO dr: drone.getValue()) {
                MedicationDTO medicationDTO = new MedicationDTO();
                medicationDTO.setId(dr.getMedicationId());
                medicationDTO.setCode(dr.getCode());
                medicationDTO.setName(dr.getName());
                medicationDTO.setWeight(dr.getWeight());

                medicationDTOList.add(medicationDTO);
            }
            droneMedicationDTO.setMedication(medicationDTOList);

            droneMedicationDTOS.add(droneMedicationDTO);
        }

        return droneMedicationDTOS;
    }
}
