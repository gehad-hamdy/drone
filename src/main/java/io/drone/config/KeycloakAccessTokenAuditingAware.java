package io.drone.config;

import org.keycloak.representations.AccessToken;
import org.keycloak.representations.JsonWebToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Optional;

@Component
public class KeycloakAccessTokenAuditingAware implements AuditorAware<String> {

    @Autowired
    private AccessToken accessToken;

    @Override
    public Optional<String> getCurrentAuditor() {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.accessToken).map(JsonWebToken::getId);
    }
}
