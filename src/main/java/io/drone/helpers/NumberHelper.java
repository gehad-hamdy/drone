package io.drone.helpers;


public class NumberHelper {
    private NumberHelper() {
    }

    public static boolean isIntegerValue(final double d) {
        return (d % 1) == 0;
    }
}
