package io.drone.data.repositories;

import io.drone.data.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long>, JpaSpecificationExecutor<Medication> {
    @Query(value = "select * from medication where drone_id is null ORDER BY weight DESC ", nativeQuery = true)
    Optional<List<Medication>> getNotAssignedMedication();

    @Query(value = "select medication.* from medication join drones on drones.id = medication.drone_id where drones.state = :state and drone_id = :droneId ", nativeQuery = true)
    Optional<List<Medication>> findMedicationByDrone(final String state, final Long droneId);

    @Query(value = "SELECT coalesce(max(id), 0) FROM medication" , nativeQuery = true)
    Long getMaxId();
}
