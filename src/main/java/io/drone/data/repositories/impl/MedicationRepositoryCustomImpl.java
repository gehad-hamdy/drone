package io.drone.data.repositories.impl;

import io.drone.api.dto.DroneMedicationDTO;
import io.drone.api.dto.DroneMedicationFlattenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.sql.JDBCType;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class MedicationRepositoryCustomImpl {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

   public void updateMedicationsByDroneIds(final Map<Long, Long> medicationDroneIDs){
      List<String> values = new ArrayList<>();
       for (Map.Entry<Long, Long> ids: medicationDroneIDs.entrySet()) {
           final String QUERY = "UPDATE medication set drone_id = :drone_id " +
                   "WHERE medication.id = :medication_id";
           this.jdbcTemplate.getJdbcTemplate().execute((ConnectionCallback<Void>) connection -> {
               this.jdbcTemplate.update(QUERY,
                       new MapSqlParameterSource()
                               .addValue("medication_id", ids.getKey())
                               .addValue("drone_id", ids.getValue())
               );
               return null;
           });
       }
    }

    public List<DroneMedicationFlattenDTO> listDronesWithMedication(){
       String query = "select drones.id as drone_id, " +
               "       drones.serial_number, " +
               "       drones.battery_capacity," +
               "       drones.model," +
               "       drones.state , " +
               "       drones.weight_limit , " +
               "       medication.id as medication_id, " +
               "       medication.name , " +
               "       medication.code, " +
               "       medication.weight" +
               "  from drones " +
               "  left join medication on medication.drone_id = drones.id " +
               "  WHERE medication.drone_id is not null";

        return this.jdbcTemplate.getJdbcTemplate().execute((ConnectionCallback<List<DroneMedicationFlattenDTO>>) connection -> {
            return this.jdbcTemplate.query(query,
                    new MapSqlParameterSource()
                    , BeanPropertyRowMapper.newInstance(DroneMedicationFlattenDTO.class)
            );
        });
    }
}
