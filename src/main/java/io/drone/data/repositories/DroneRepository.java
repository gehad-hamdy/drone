package io.drone.data.repositories;


import io.drone.data.entities.Drone;
import io.drone.enums.StateTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long>, JpaSpecificationExecutor<Drone> {
    List<Drone> findByState(@NotNull StateTypeEnum state);

    List<Drone> findByStateAndBatteryCapacityIsGreaterThanEqualOrderByWeightLimitDesc(@NotNull StateTypeEnum state, Integer capacity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE drones SET state = :state WHERE id in (:droneIDs)", nativeQuery = true)
    void updateDroneStateByIds(final String state, final List<Long> droneIDs);

    @Query(value = "SELECT coalesce(max(id), 0) FROM drones" , nativeQuery = true)
    Long getMaxId();
}
