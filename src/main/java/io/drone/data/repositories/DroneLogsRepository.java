package io.drone.data.repositories;

import io.drone.data.entities.DroneLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneLogsRepository extends JpaRepository<DroneLogs, Long>, JpaSpecificationExecutor<DroneLogs> {
}
