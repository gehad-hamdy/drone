package io.drone.api.mappers;

import io.drone.api.v1.resources.MedicationResponseBodyResource;
import io.drone.data.entities.Medication;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicationMapper {
    MedicationResponseBodyResource map(Medication medication);

    List<MedicationResponseBodyResource> map(List<Medication> medications);
}
