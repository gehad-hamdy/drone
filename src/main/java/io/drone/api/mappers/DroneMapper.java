package io.drone.api.mappers;

import io.drone.api.v1.resources.DroneRequestBodyResource;
import io.drone.api.v1.resources.DroneResponseResource;
import io.drone.api.v1.resources.ModelStatusResource;
import io.drone.api.v1.resources.StateStatusResource;
import io.drone.data.entities.Drone;
import io.drone.enums.ModelTypeEnum;
import io.drone.enums.StateTypeEnum;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring")
public interface DroneMapper {
    @ValueMappings({
        @ValueMapping(source = "LIGHTWEIGHT", target = "Lightweight"),
        @ValueMapping(source = "MIDDLEWEIGHT", target = "Middleweight"),
        @ValueMapping(source = "CRUISERWEIGHT", target = "Cruiserweight"),
        @ValueMapping(source = "HEAVYWEIGHT", target = "Heavyweight")
    })
    ModelTypeEnum map(ModelStatusResource modelStatusResource);
    StateTypeEnum map(StateStatusResource statusResource);

    Drone map(DroneRequestBodyResource droneRequestBodyResource);
    DroneResponseResource map(Drone drone);

    List<DroneResponseResource> map(List<Drone> drone);
}
