package io.drone.api.mappers;

import io.drone.api.dto.DroneMedicationDTO;
import io.drone.api.dto.MedicationDTO;
import io.drone.api.v1.resources.DroneWithMedicationResource;
import io.drone.api.v1.resources.MedicationResponseBodyResource;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface DroneMedicationMapper {
    List<DroneWithMedicationResource> map(List<DroneMedicationDTO> droneMedicationDTO);

    DroneWithMedicationResource map(DroneMedicationDTO droneMedicationDTO);

    MedicationResponseBodyResource map(MedicationDTO medicationDTO);
}
