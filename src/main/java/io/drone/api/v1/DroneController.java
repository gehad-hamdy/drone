package io.drone.api.v1;

import io.drone.api.mappers.DroneMapper;
import io.drone.api.mappers.DroneMedicationMapper;
import io.drone.api.v1.resources.DroneRequestBodyResource;
import io.drone.api.v1.resources.DroneResponseResource;
import io.drone.api.v1.resources.DroneWithMedicationResource;
import io.drone.api.v1.resources.StateStatusResource;
import io.drone.services.DroneService;
import io.drone.services.Validations;
import io.drone.services.exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DroneController extends AbstractController implements DronesApi{
    @Autowired
    private DroneService droneService;

    @Autowired
    private Validations validations;

    @Autowired
    private DroneMapper droneMapper;

    @Autowired
    private DroneMedicationMapper droneMedicationMapper;

    @Override
    public ResponseEntity<DroneResponseResource> registerDrone(DroneRequestBodyResource droneRequestBodyResource) throws BusinessException{
        try {
            this.validations.validateAbstractDrone(droneRequestBodyResource);
            var drone = this.droneService.registerDrone(this.droneMapper.map(droneRequestBodyResource));
            return ResponseEntity
                    .ok()
                    .body(this.droneMapper.map(drone));
        }catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<DroneResponseResource> getDroneById(Long id) throws BusinessException {
        try {
            return ResponseEntity
                    .ok()
                    .body(this.droneMapper.map(this.droneService.getDroneById(id)));
        }catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<List<DroneResponseResource>> listDronesByState(StateStatusResource state) throws BusinessException {
        try {
            var drones = this.droneService.listDronesByState(state.getValue());
            return ResponseEntity
                    .ok()
                    .body(this.droneMapper.map(drones));
        }catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<List<DroneWithMedicationResource>> getDroneWithMedicationItemsAssigned() {
       try {
           var drones = this.droneService.listDronesWithMedication();
           return ResponseEntity
                   .ok()
                   .body(this.droneMedicationMapper.map(drones));
       }catch (final Exception e) {
           throw new RuntimeException(e);
       }
    }

}
