package io.drone.api.v1;

import io.drone.api.mappers.MedicationMapper;
import io.drone.api.v1.resources.DroneResponseResource;
import io.drone.api.v1.resources.MedicationResponseBodyResource;
import io.drone.services.MedicationService;
import io.drone.services.Validations;
import io.drone.services.exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class MedicationController extends AbstractController implements MedicationApi{
    @Autowired
    private MedicationService medicationService;

    @Autowired
    private Validations validations;

    @Autowired
    private MedicationMapper medicationMapper;

    @Override
    public ResponseEntity<MedicationResponseBodyResource> addMedication(String name, Double weight, String code, MultipartFile image) throws BusinessException {
       try{
           this.validations.validateMedicationRequest(name, code, image);
           var medication = this.medicationService.addMedication(name, weight.intValue(), code, image);
           return ResponseEntity
                   .ok()
                   .body(this.medicationMapper.map(medication));
       }catch (final Exception e) {
           throw new RuntimeException(e);
       }
    }

    @Override
    public ResponseEntity<Void> assignMedicationDrone() {
        try{
            this.medicationService.assignMedicationToAvailableDrone();
        }catch (final Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public ResponseEntity<List<MedicationResponseBodyResource>> getLoadedMedicationByDroneId(Long id) {
        try{
            var medication = this.medicationService.getMedicationByDroneId(id);
            return ResponseEntity
                    .ok()
                    .body(this.medicationMapper.map(medication));
        }catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}
