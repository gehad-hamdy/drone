package io.drone.api.dto;


import java.util.List;

public class DroneMedicationDTO {
    private Long id;
    private String serialNumber;
    private String model;
    private String state;
    private Integer weightLimit;
    private Integer batteryCapacity;

    private List<MedicationDTO> medication;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Integer weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public List<MedicationDTO> getMedication() {
        return medication;
    }

    public void setMedication(List<MedicationDTO> medication) {
        this.medication = medication;
    }
}
