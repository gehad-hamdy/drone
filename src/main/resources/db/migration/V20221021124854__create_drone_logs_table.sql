create TABLE IF NOT EXISTS drone_logs (
    id int8 generated by default as identity,
    drone_id int8,
    battery_level int4 NOT NULL,
    created_at timestamp,
    updated_at timestamp,
    primary key (id)
);

alter table if exists drone_logs
    add constraint drone_logs_drone_foreign_key
    foreign key (drone_id)
    references drones;