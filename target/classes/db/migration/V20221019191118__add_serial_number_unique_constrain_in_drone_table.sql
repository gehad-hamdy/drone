ALTER TABLE drones
    ADD CONSTRAINT serial_number_unique UNIQUE (serial_number);

ALTER TABLE medication
    ADD CONSTRAINT medication_code_unique UNIQUE (code);