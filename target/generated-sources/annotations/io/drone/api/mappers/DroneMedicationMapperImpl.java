package io.drone.api.mappers;

import io.drone.api.dto.DroneMedicationDTO;
import io.drone.api.dto.MedicationDTO;
import io.drone.api.v1.resources.DroneWithMedicationResource;
import io.drone.api.v1.resources.MedicationResponseBodyResource;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-21T20:55:16+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class DroneMedicationMapperImpl implements DroneMedicationMapper {

    @Override
    public List<DroneWithMedicationResource> map(List<DroneMedicationDTO> droneMedicationDTO) {
        if ( droneMedicationDTO == null ) {
            return null;
        }

        List<DroneWithMedicationResource> list = new ArrayList<DroneWithMedicationResource>( droneMedicationDTO.size() );
        for ( DroneMedicationDTO droneMedicationDTO1 : droneMedicationDTO ) {
            list.add( map( droneMedicationDTO1 ) );
        }

        return list;
    }

    @Override
    public DroneWithMedicationResource map(DroneMedicationDTO droneMedicationDTO) {
        if ( droneMedicationDTO == null ) {
            return null;
        }

        DroneWithMedicationResource droneWithMedicationResource = new DroneWithMedicationResource();

        if ( droneMedicationDTO.getId() != null ) {
            droneWithMedicationResource.setId( droneMedicationDTO.getId().intValue() );
        }
        droneWithMedicationResource.setSerialNumber( droneMedicationDTO.getSerialNumber() );
        droneWithMedicationResource.setModel( droneMedicationDTO.getModel() );
        droneWithMedicationResource.setState( droneMedicationDTO.getState() );
        if ( droneMedicationDTO.getWeightLimit() != null ) {
            droneWithMedicationResource.setWeightLimit( droneMedicationDTO.getWeightLimit().doubleValue() );
        }
        droneWithMedicationResource.setBatteryCapacity( droneMedicationDTO.getBatteryCapacity() );
        droneWithMedicationResource.setMedication( medicationDTOListToMedicationResponseBodyResourceList( droneMedicationDTO.getMedication() ) );

        return droneWithMedicationResource;
    }

    @Override
    public MedicationResponseBodyResource map(MedicationDTO medicationDTO) {
        if ( medicationDTO == null ) {
            return null;
        }

        MedicationResponseBodyResource medicationResponseBodyResource = new MedicationResponseBodyResource();

        if ( medicationDTO.getId() != null ) {
            medicationResponseBodyResource.setId( medicationDTO.getId().intValue() );
        }
        medicationResponseBodyResource.setName( medicationDTO.getName() );
        if ( medicationDTO.getWeight() != null ) {
            medicationResponseBodyResource.setWeight( medicationDTO.getWeight().doubleValue() );
        }
        medicationResponseBodyResource.setCode( medicationDTO.getCode() );

        return medicationResponseBodyResource;
    }

    protected List<MedicationResponseBodyResource> medicationDTOListToMedicationResponseBodyResourceList(List<MedicationDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<MedicationResponseBodyResource> list1 = new ArrayList<MedicationResponseBodyResource>( list.size() );
        for ( MedicationDTO medicationDTO : list ) {
            list1.add( map( medicationDTO ) );
        }

        return list1;
    }
}
