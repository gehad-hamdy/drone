package io.drone.api.mappers;

import io.drone.api.v1.resources.MedicationResponseBodyResource;
import io.drone.data.entities.Medication;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-21T20:55:16+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class MedicationMapperImpl implements MedicationMapper {

    @Override
    public MedicationResponseBodyResource map(Medication medication) {
        if ( medication == null ) {
            return null;
        }

        MedicationResponseBodyResource medicationResponseBodyResource = new MedicationResponseBodyResource();

        if ( medication.getId() != null ) {
            medicationResponseBodyResource.setId( medication.getId().intValue() );
        }
        medicationResponseBodyResource.setName( medication.getName() );
        if ( medication.getWeight() != null ) {
            medicationResponseBodyResource.setWeight( medication.getWeight().doubleValue() );
        }
        medicationResponseBodyResource.setCode( medication.getCode() );
        medicationResponseBodyResource.setImage( medication.getImage() );

        return medicationResponseBodyResource;
    }

    @Override
    public List<MedicationResponseBodyResource> map(List<Medication> medications) {
        if ( medications == null ) {
            return null;
        }

        List<MedicationResponseBodyResource> list = new ArrayList<MedicationResponseBodyResource>( medications.size() );
        for ( Medication medication : medications ) {
            list.add( map( medication ) );
        }

        return list;
    }
}
