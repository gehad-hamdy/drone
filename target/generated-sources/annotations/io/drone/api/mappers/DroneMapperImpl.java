package io.drone.api.mappers;

import io.drone.api.v1.resources.DroneRequestBodyResource;
import io.drone.api.v1.resources.DroneResponseResource;
import io.drone.api.v1.resources.ModelStatusResource;
import io.drone.api.v1.resources.StateStatusResource;
import io.drone.data.entities.Drone;
import io.drone.enums.ModelTypeEnum;
import io.drone.enums.StateTypeEnum;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-21T20:55:16+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class DroneMapperImpl implements DroneMapper {

    @Override
    public ModelTypeEnum map(ModelStatusResource modelStatusResource) {
        if ( modelStatusResource == null ) {
            return null;
        }

        ModelTypeEnum modelTypeEnum;

        switch ( modelStatusResource ) {
            case LIGHTWEIGHT: modelTypeEnum = ModelTypeEnum.Lightweight;
            break;
            case MIDDLEWEIGHT: modelTypeEnum = ModelTypeEnum.Middleweight;
            break;
            case CRUISERWEIGHT: modelTypeEnum = ModelTypeEnum.Cruiserweight;
            break;
            case HEAVYWEIGHT: modelTypeEnum = ModelTypeEnum.Heavyweight;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + modelStatusResource );
        }

        return modelTypeEnum;
    }

    @Override
    public StateTypeEnum map(StateStatusResource statusResource) {
        if ( statusResource == null ) {
            return null;
        }

        StateTypeEnum stateTypeEnum;

        switch ( statusResource ) {
            case IDLE: stateTypeEnum = StateTypeEnum.IDLE;
            break;
            case LOADING: stateTypeEnum = StateTypeEnum.LOADING;
            break;
            case LOADED: stateTypeEnum = StateTypeEnum.LOADED;
            break;
            case DELIVERING: stateTypeEnum = StateTypeEnum.DELIVERING;
            break;
            case DELIVERED: stateTypeEnum = StateTypeEnum.DELIVERED;
            break;
            case RETURNING: stateTypeEnum = StateTypeEnum.RETURNING;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + statusResource );
        }

        return stateTypeEnum;
    }

    @Override
    public Drone map(DroneRequestBodyResource droneRequestBodyResource) {
        if ( droneRequestBodyResource == null ) {
            return null;
        }

        Drone drone = new Drone();

        drone.setSerialNumber( droneRequestBodyResource.getSerialNumber() );
        drone.setModel( map( droneRequestBodyResource.getModel() ) );
        drone.setState( map( droneRequestBodyResource.getState() ) );
        if ( droneRequestBodyResource.getWeightLimit() != null ) {
            drone.setWeightLimit( droneRequestBodyResource.getWeightLimit().intValue() );
        }
        drone.setBatteryCapacity( droneRequestBodyResource.getBatteryCapacity() );

        return drone;
    }

    @Override
    public DroneResponseResource map(Drone drone) {
        if ( drone == null ) {
            return null;
        }

        DroneResponseResource droneResponseResource = new DroneResponseResource();

        if ( drone.getId() != null ) {
            droneResponseResource.setId( drone.getId().intValue() );
        }
        droneResponseResource.setSerialNumber( drone.getSerialNumber() );
        if ( drone.getModel() != null ) {
            droneResponseResource.setModel( drone.getModel().name() );
        }
        if ( drone.getState() != null ) {
            droneResponseResource.setState( drone.getState().name() );
        }
        if ( drone.getWeightLimit() != null ) {
            droneResponseResource.setWeightLimit( drone.getWeightLimit().doubleValue() );
        }
        droneResponseResource.setBatteryCapacity( drone.getBatteryCapacity() );

        return droneResponseResource;
    }

    @Override
    public List<DroneResponseResource> map(List<Drone> drone) {
        if ( drone == null ) {
            return null;
        }

        List<DroneResponseResource> list = new ArrayList<DroneResponseResource>( drone.size() );
        for ( Drone drone1 : drone ) {
            list.add( map( drone1 ) );
        }

        return list;
    }
}
