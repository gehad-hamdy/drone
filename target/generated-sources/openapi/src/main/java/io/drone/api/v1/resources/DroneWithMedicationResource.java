package io.drone.api.v1.resources;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.drone.api.v1.resources.MedicationResponseBodyResource;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * DroneWithMedicationResource
 */

@JsonTypeName("droneWithMedication")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-21T20:55:12.888643617+02:00[Africa/Cairo]")
public class DroneWithMedicationResource {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("serialNumber")
  private String serialNumber;

  @JsonProperty("model")
  private String model;

  @JsonProperty("state")
  private String state;

  @JsonProperty("weightLimit")
  private Double weightLimit;

  @JsonProperty("batteryCapacity")
  private Integer batteryCapacity;

  @JsonProperty("medication")
  @Valid
  private List<MedicationResponseBodyResource> medication = null;

  public DroneWithMedicationResource id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DroneWithMedicationResource serialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  /**
   * Get serialNumber
   * @return serialNumber
  */
  
  @Schema(name = "serialNumber", required = false)
  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public DroneWithMedicationResource model(String model) {
    this.model = model;
    return this;
  }

  /**
   * Get model
   * @return model
  */
  
  @Schema(name = "model", required = false)
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public DroneWithMedicationResource state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  */
  
  @Schema(name = "state", required = false)
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public DroneWithMedicationResource weightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
    return this;
  }

  /**
   * Get weightLimit
   * @return weightLimit
  */
  
  @Schema(name = "weightLimit", required = false)
  public Double getWeightLimit() {
    return weightLimit;
  }

  public void setWeightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
  }

  public DroneWithMedicationResource batteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
    return this;
  }

  /**
   * Get batteryCapacity
   * @return batteryCapacity
  */
  
  @Schema(name = "batteryCapacity", required = false)
  public Integer getBatteryCapacity() {
    return batteryCapacity;
  }

  public void setBatteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
  }

  public DroneWithMedicationResource medication(List<MedicationResponseBodyResource> medication) {
    this.medication = medication;
    return this;
  }

  public DroneWithMedicationResource addMedicationItem(MedicationResponseBodyResource medicationItem) {
    if (this.medication == null) {
      this.medication = new ArrayList<>();
    }
    this.medication.add(medicationItem);
    return this;
  }

  /**
   * Get medication
   * @return medication
  */
  @Valid 
  @Schema(name = "medication", required = false)
  public List<MedicationResponseBodyResource> getMedication() {
    return medication;
  }

  public void setMedication(List<MedicationResponseBodyResource> medication) {
    this.medication = medication;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DroneWithMedicationResource droneWithMedication = (DroneWithMedicationResource) o;
    return Objects.equals(this.id, droneWithMedication.id) &&
        Objects.equals(this.serialNumber, droneWithMedication.serialNumber) &&
        Objects.equals(this.model, droneWithMedication.model) &&
        Objects.equals(this.state, droneWithMedication.state) &&
        Objects.equals(this.weightLimit, droneWithMedication.weightLimit) &&
        Objects.equals(this.batteryCapacity, droneWithMedication.batteryCapacity) &&
        Objects.equals(this.medication, droneWithMedication.medication);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, serialNumber, model, state, weightLimit, batteryCapacity, medication);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DroneWithMedicationResource {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    serialNumber: ").append(toIndentedString(serialNumber)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    weightLimit: ").append(toIndentedString(weightLimit)).append("\n");
    sb.append("    batteryCapacity: ").append(toIndentedString(batteryCapacity)).append("\n");
    sb.append("    medication: ").append(toIndentedString(medication)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

