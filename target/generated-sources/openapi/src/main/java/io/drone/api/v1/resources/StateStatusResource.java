package io.drone.api.v1.resources;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets stateStatus
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-21T20:55:12.888643617+02:00[Africa/Cairo]")
public enum StateStatusResource {
  
  IDLE("IDLE"),
  
  LOADING("LOADING"),
  
  LOADED("LOADED"),
  
  DELIVERING("DELIVERING"),
  
  DELIVERED("DELIVERED"),
  
  RETURNING("RETURNING");

  private String value;

  StateStatusResource(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static StateStatusResource fromValue(String value) {
    for (StateStatusResource b : StateStatusResource.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

