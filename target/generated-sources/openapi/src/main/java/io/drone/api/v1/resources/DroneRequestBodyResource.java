package io.drone.api.v1.resources;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.drone.api.v1.resources.ModelStatusResource;
import io.drone.api.v1.resources.StateStatusResource;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * DroneRequestBodyResource
 */

@JsonTypeName("droneRequestBody")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-21T20:55:12.888643617+02:00[Africa/Cairo]")
public class DroneRequestBodyResource {

  @JsonProperty("serial_number")
  private String serialNumber;

  @JsonProperty("model")
  private ModelStatusResource model;

  @JsonProperty("weight_limit")
  private Double weightLimit;

  @JsonProperty("battery_capacity")
  private Integer batteryCapacity;

  @JsonProperty("state")
  private StateStatusResource state;

  public DroneRequestBodyResource serialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  /**
   * Get serialNumber
   * @return serialNumber
  */
  
  @Schema(name = "serial_number", required = false)
  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public DroneRequestBodyResource model(ModelStatusResource model) {
    this.model = model;
    return this;
  }

  /**
   * Get model
   * @return model
  */
  @Valid 
  @Schema(name = "model", required = false)
  public ModelStatusResource getModel() {
    return model;
  }

  public void setModel(ModelStatusResource model) {
    this.model = model;
  }

  public DroneRequestBodyResource weightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
    return this;
  }

  /**
   * Get weightLimit
   * maximum: 500
   * @return weightLimit
  */
  @DecimalMax("500") 
  @Schema(name = "weight_limit", required = false)
  public Double getWeightLimit() {
    return weightLimit;
  }

  public void setWeightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
  }

  public DroneRequestBodyResource batteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
    return this;
  }

  /**
   * Get batteryCapacity
   * maximum: 100
   * @return batteryCapacity
  */
  @Max(100) 
  @Schema(name = "battery_capacity", required = false)
  public Integer getBatteryCapacity() {
    return batteryCapacity;
  }

  public void setBatteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
  }

  public DroneRequestBodyResource state(StateStatusResource state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  */
  @Valid 
  @Schema(name = "state", required = false)
  public StateStatusResource getState() {
    return state;
  }

  public void setState(StateStatusResource state) {
    this.state = state;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DroneRequestBodyResource droneRequestBody = (DroneRequestBodyResource) o;
    return Objects.equals(this.serialNumber, droneRequestBody.serialNumber) &&
        Objects.equals(this.model, droneRequestBody.model) &&
        Objects.equals(this.weightLimit, droneRequestBody.weightLimit) &&
        Objects.equals(this.batteryCapacity, droneRequestBody.batteryCapacity) &&
        Objects.equals(this.state, droneRequestBody.state);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serialNumber, model, weightLimit, batteryCapacity, state);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DroneRequestBodyResource {\n");
    sb.append("    serialNumber: ").append(toIndentedString(serialNumber)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    weightLimit: ").append(toIndentedString(weightLimit)).append("\n");
    sb.append("    batteryCapacity: ").append(toIndentedString(batteryCapacity)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

