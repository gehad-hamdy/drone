package io.drone.api.v1.resources;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * DroneResponseResource
 */

@JsonTypeName("droneResponse")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-21T20:55:12.888643617+02:00[Africa/Cairo]")
public class DroneResponseResource {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("serialNumber")
  private String serialNumber;

  @JsonProperty("model")
  private String model;

  @JsonProperty("state")
  private String state;

  @JsonProperty("weightLimit")
  private Double weightLimit;

  @JsonProperty("batteryCapacity")
  private Integer batteryCapacity;

  public DroneResponseResource id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DroneResponseResource serialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  /**
   * Get serialNumber
   * @return serialNumber
  */
  
  @Schema(name = "serialNumber", required = false)
  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public DroneResponseResource model(String model) {
    this.model = model;
    return this;
  }

  /**
   * Get model
   * @return model
  */
  
  @Schema(name = "model", required = false)
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public DroneResponseResource state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  */
  
  @Schema(name = "state", required = false)
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public DroneResponseResource weightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
    return this;
  }

  /**
   * Get weightLimit
   * @return weightLimit
  */
  
  @Schema(name = "weightLimit", required = false)
  public Double getWeightLimit() {
    return weightLimit;
  }

  public void setWeightLimit(Double weightLimit) {
    this.weightLimit = weightLimit;
  }

  public DroneResponseResource batteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
    return this;
  }

  /**
   * Get batteryCapacity
   * @return batteryCapacity
  */
  
  @Schema(name = "batteryCapacity", required = false)
  public Integer getBatteryCapacity() {
    return batteryCapacity;
  }

  public void setBatteryCapacity(Integer batteryCapacity) {
    this.batteryCapacity = batteryCapacity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DroneResponseResource droneResponse = (DroneResponseResource) o;
    return Objects.equals(this.id, droneResponse.id) &&
        Objects.equals(this.serialNumber, droneResponse.serialNumber) &&
        Objects.equals(this.model, droneResponse.model) &&
        Objects.equals(this.state, droneResponse.state) &&
        Objects.equals(this.weightLimit, droneResponse.weightLimit) &&
        Objects.equals(this.batteryCapacity, droneResponse.batteryCapacity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, serialNumber, model, state, weightLimit, batteryCapacity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DroneResponseResource {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    serialNumber: ").append(toIndentedString(serialNumber)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    weightLimit: ").append(toIndentedString(weightLimit)).append("\n");
    sb.append("    batteryCapacity: ").append(toIndentedString(batteryCapacity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

