package io.drone.api.v1.resources;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ConstraintViolationAllOfResource
 */

@JsonTypeName("ConstraintViolation_allOf")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-21T20:55:12.888643617+02:00[Africa/Cairo]")
public class ConstraintViolationAllOfResource {

  @JsonProperty("property")
  private String property;

  public ConstraintViolationAllOfResource property(String property) {
    this.property = property;
    return this;
  }

  /**
   * Property name
   * @return property
  */
  
  @Schema(name = "property", description = "Property name", required = false)
  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConstraintViolationAllOfResource constraintViolationAllOf = (ConstraintViolationAllOfResource) o;
    return Objects.equals(this.property, constraintViolationAllOf.property);
  }

  @Override
  public int hashCode() {
    return Objects.hash(property);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConstraintViolationAllOfResource {\n");
    sb.append("    property: ").append(toIndentedString(property)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

