# MaxAB Java Template Project

## Prerequisites
* PostgreSQL if you need use it you should un comment the application properties

  Edit `application-default.properties` and `application-test.properties` and set the correct database name and create
  the database using commands like this:
    ```sql
    create role drone with login password 'drone';
    create database drones with owner drone;
    create database "drones-tests" with owner drone;
    ```

* JDK 17 ([Amazon Corretto](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)).

## run application
* I'm adding keycloak app security and adding 2 roles (admin,user)
* you can follow the link to setup it locally https://www.baeldung.com/spring-boot-keycloak
* maven install
* data will be loaded
* we have schedule job to run every 15 minutes to log the drone battery level
* we have multiple api to check the drone register and the relation between drones and medication

## Running Tests
* every config is setup well